//
//  ViewController.m
//  test
//
//  Created by ChoHeeTae on 12. 5. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//////

#import "ViewController.h"
#import "ImageFilter.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *testImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"test.png"]];
    
    testImageView.image = [[UIImage imageNamed:@"test.png"] sharpen];
    
    [self.view addSubview:testImageView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
